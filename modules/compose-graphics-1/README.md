# Module 6.1 Jetpack Compose Graphics 1

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 2.2: Android Databases Using Room](../modules/room)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

Building custom components with Jetpack Compose is significantly easier than it used to be with the old View approach!

In this module, we'll take a quick look at the old way and compare it to Compose, then dive in for some progressively more complex custom components, drawing everything ourselves to create exactly the UI we would like.

## Objectives

* Understand how to use a Canvas to draw custom components and display drawings on buttons
* Create simple animation by varying the inputs to a Composable function over time
* Respond to user tap interaction to place new items on the screen

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:47:55

            
#### Jetpack Compose Graphics: Introduction (Fall 2021) (06:50)
 | [![Jetpack Compose Graphics: Introduction (Fall 2021)](https://img.youtube.com/vi/1t41a-X8Ewc/mqdefault.jpg)](https://youtu.be/1t41a-X8Ewc) |
 | -- |

                

#### Jetpack Compose Graphics: A Simple Gauge (Fall 2021) (24:32)
 | [![Jetpack Compose Graphics: A Simple Gauge (Fall 2021)](https://img.youtube.com/vi/BpVQus2r4qs/mqdefault.jpg)](https://youtu.be/BpVQus2r4qs) |
 | -- |

                

#### Jetpack Compose Graphics: An Analog Clock (Fall 2021) (20:00)
 | [![Jetpack Compose Graphics: An Analog Clock (Fall 2021)](https://img.youtube.com/vi/5FtqH8dv0BM/mqdefault.jpg)](https://youtu.be/5FtqH8dv0BM) |
 | -- |

                

#### Jetpack Compose Graphics: Graph Editor, Part 1 (Fall 2021) (56:33)
 | [![Jetpack Compose Graphics: Graph Editor, Part 1 (Fall 2021)](https://img.youtube.com/vi/kNLr8Oe8vnA/mqdefault.jpg)](https://youtu.be/kNLr8Oe8vnA) |
 | -- |

                

            
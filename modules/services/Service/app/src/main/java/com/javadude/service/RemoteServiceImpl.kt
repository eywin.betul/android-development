package com.javadude.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

// implementation of our AIDL-based service
// the binder object we define implements the AIDL interface by subclassing its generated stub
class RemoteServiceImpl : Service() {
    private val reporters = mutableListOf<RemoteServiceReporter>()
    private val scope = CoroutineScope(Dispatchers.IO)
    private var job: Job? = null

    @Volatile var i = 1

    // the api we expose to the client
    private var binder = object : RemoteService.Stub() {
        override fun reset() {
            i = 1
        }
        override fun add(reporter : RemoteServiceReporter) {
            reporters.add(reporter)
        }
        override fun remove(reporter : RemoteServiceReporter) {
            reporters.remove(reporter)
        }
    }


    override fun onDestroy() {
        job?.cancel()
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder {
        // if not already launched, launch a coroutine to start counting
        if (job == null) {
            job = scope.launch {
                while(true) {
                    Log.d("StartedService", "count = $i")
                    val people = listOf(
                        Person("Jenny", 10 + i),
                        Person("Tommy", 5 + i)
                    )
                    reporters.forEach {
                        it.report(people, i)
                    }
                    delay(250)
                    i++
                }
            }
        }
        return binder
    }
}
package com.javadude.widgets

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.RoomDatabase
import kotlinx.coroutines.flow.Flow
import java.util.UUID

@Entity
class Person {
    @PrimaryKey var id : String = UUID.randomUUID().toString()
    var name : String = ""
}

@Dao
interface PersonDao {
    @Query("SELECT * FROM Person ORDER BY name")
    fun getAll() : Flow<List<Person>>

    @Query("SELECT * FROM Person ORDER BY name")
    fun getAllSync() : List<Person>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(vararg people : Person)
}

@Database(version = 1, entities = [Person::class])
abstract class PersonDatabase : RoomDatabase() {
    abstract val personDao : PersonDao
}

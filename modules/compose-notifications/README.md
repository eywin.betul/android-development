# Module 14.2 Workers

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.3: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
* [Module 10.2: Android Services](../modules/services)
                
<!-- END COMMON HEADER -->

## Introduction

(TBD)

## Objectives

(TBD)

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 00:00

(Videos in development)

            
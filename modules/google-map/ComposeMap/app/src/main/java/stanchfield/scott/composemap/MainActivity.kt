package stanchfield.scott.composemap

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.google.android.libraries.maps.CameraUpdateFactory
import com.google.android.libraries.maps.MapView
import com.google.android.libraries.maps.model.LatLng
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.awaitMap
import kotlinx.coroutines.launch
import stanchfield.scott.composemap.ui.theme.ComposeMapTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val latitude = 39.163400392993395
        val longitude = -76.89197363588868

        setContent {
            ComposeMapTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Scaffold(
                        topBar = {
                            TopAppBar(
                                title = { Text(stringResource(id = R.string.app_name)) }
                            )
                        },
                        content = {
                            val mapView = remember {
                                MapView(this).apply {
                                    id = R.id.google_map
                                }
                            }

                            DisposableEffect(lifecycle, mapView) {
                                val lifecycleObserver =
                                    LifecycleEventObserver { _, event ->
                                        when (event) {
                                            Lifecycle.Event.ON_CREATE -> mapView.onCreate(savedInstanceState)
                                            Lifecycle.Event.ON_START -> mapView.onStart()
                                            Lifecycle.Event.ON_RESUME -> mapView.onResume()
                                            Lifecycle.Event.ON_STOP -> mapView.onStop()
                                            Lifecycle.Event.ON_PAUSE -> mapView.onPause()
                                            Lifecycle.Event.ON_DESTROY -> mapView.onDestroy()
                                            else -> throw IllegalStateException("Unhandled lifecycle event")
                                        }
                                    }
                                lifecycle.addObserver(lifecycleObserver)
                                onDispose {
                                    lifecycle.removeObserver(lifecycleObserver)
                                }
                            }

                            val cameraPosition = remember(latitude, longitude) {
                                LatLng(latitude, longitude)
                            }

                            val scope = rememberCoroutineScope()

                            AndroidView(
                                factory = { mapView },
                                update = {
                                    scope.launch {
                                        val googleMap = it.awaitMap()
                                        googleMap.addMarker { position(cameraPosition) }
                                        googleMap.animateCamera(
                                            CameraUpdateFactory.newLatLngZoom(cameraPosition, 15f),
                                            1000, null
                                        )
                                    }
                                }
                            )
                        }
                    )
                }
            }
        }
    }
}

# Module 2.1 Gradle Basics

            
<!-- END COMMON HEADER -->

## Introduction

(TBD)

## Objectives

(TBD)

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 00:00

(Videos in development)

            
package com.javadude.movies2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.LocalConfiguration
import com.javadude.movies2.ui.theme.Movies2Theme
import kotlinx.coroutines.launch

@ExperimentalComposeUiApi
@ExperimentalMaterialApi
class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Movies2Theme {
                val configuration = LocalConfiguration.current
                val isWide = configuration.screenWidthDp >= 700

                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Ui(isWide, viewModel) {
                        finish()
                    }
                }
            }
        }
    }
}

@ExperimentalComposeUiApi
@ExperimentalMaterialApi
@Composable
fun Ui(
    isWide: Boolean,
    viewModel: MovieViewModel,
    onExit: () -> Unit
) {
    // The Ui level is the only one to touch the view model directly
    // It pulls data from the view model and manages navigation (push/pop stack)
    // All lower levels only act on data/functions passed to them
    BackHandler {
        viewModel.pop()
    }
    val movies by viewModel.movies.collectAsState(initial = emptyList())
    val actors by viewModel.actors.collectAsState(initial = emptyList())
    val currentScreen by viewModel.currentScreen.collectAsState(initial = MovieListScreen(NoListSelection))
    val scope = rememberCoroutineScope()

    when(val screen = currentScreen) {
        is MovieListScreen ->
            MovieList(
                isWide = isWide,
                movies = movies,
                selection = screen.selection,
                onSelectionChange = {
                    when (it) {
                        NoListSelection, is MultiListSelection -> {
                            viewModel.replaceTop(MovieListScreen(it))
                        }
                        is SingleListSelection -> {
                            viewModel.replaceTop(MovieListScreen(NoListSelection))
                            viewModel.push(MovieDisplayScreen(it))
                        }
                    }
                },
                onAddMovie = {
                    scope.launch {
                        val movie = viewModel.createMovie()
                        viewModel.push(MovieEditScreen(SingleListSelection(movie.id)))
                    }
                },
                onDeleteMovies = {
                    scope.launch {
                        viewModel.deleteMoviesById(it.ids)
                        viewModel.replaceTop(MovieListScreen(NoListSelection))
                    }
                },
                onResetDb = {
                    scope.launch {
                        viewModel.resetDatabase()
                    }
                }
            )
        is MovieDisplayScreen -> {
            if (isWide) {
                BackHandler {
                    if (viewModel.peekOneBack() is MovieListScreen) {
                        onExit()
                    } else {
                        viewModel.pop()
                    }
                }
            }
            val movieId = when (screen.selection) {
                NoListSelection -> throw IllegalStateException("Cannot have a MovieDisplayScreen without a selection")
                is MultiListSelection -> throw IllegalStateException("Cannot have a MovieDisplayScreen with a multi selection")
                is SingleListSelection -> screen.selection.id
            }
            MovieDisplay(
                isWide = isWide,
                selection = screen.selection,
                movies = movies,
                onEditMovie = {
                    viewModel.push(MovieEditScreen(SingleListSelection(movieId)))
                },
                getMovie = { id -> viewModel.getMovie(id) },
                getCast = { id -> viewModel.getCast(id) },
                onSelectionChange = {
                    when (it) {
                        NoListSelection, is MultiListSelection -> {
                            viewModel.replaceTop(MovieListScreen(it))
                        }
                        is SingleListSelection -> {
                            viewModel.replaceTop(MovieListScreen(NoListSelection))
                            viewModel.push(MovieDisplayScreen(it))
                        }
                    }
                },
                onAddMovie = {
                    scope.launch {
                        val movie = viewModel.createMovie()
                        viewModel.push(MovieEditScreen(SingleListSelection(movie.id)))
                    }
                },
                onDeleteMovies = {
                    scope.launch {
                        viewModel.deleteMoviesById(it.ids)
                        viewModel.replaceTop(MovieListScreen(NoListSelection))
                    }
                }
            )
        }
        is MovieEditScreen -> {
            MovieEdit(
                isWide = isWide,
                movies = movies,
                selection = screen.selection,
                onMovieChange = { movie ->
                    scope.launch {
                        viewModel.update(movie)
                    }
                },
                getMovie = { id -> viewModel.getMovie(id) },
                onSelectionChange = {
                    when (it) {
                        NoListSelection, is MultiListSelection -> {
                            viewModel.replaceTop(MovieListScreen(it))
                        }
                        is SingleListSelection -> {
                            viewModel.replaceTop(MovieListScreen(NoListSelection))
                            viewModel.push(MovieDisplayScreen(it))
                        }
                    }
                },
                onAddMovie = {
                    scope.launch {
                        val movie = viewModel.createMovie()
                        viewModel.push(MovieEditScreen(SingleListSelection(movie.id)))
                    }
                },
                onDeleteMovies = {
                    scope.launch {
                        viewModel.deleteMoviesById(it.ids)
                        viewModel.replaceTop(MovieListScreen(NoListSelection))
                    }
                },
                onBack = { viewModel.pop() }
            )
        }
        is ActorListScreen -> Text(text = "")
//            ActorList(actors = actors)
        is ActorDisplayScreen -> Text(text = "")
//            ActorDisplay(
//                actor = screen.actor
//            )
        is ActorEditScreen -> Text(text = "")
//            ActorEdit(
//                actor = screen.actor
//            )
        null -> onExit()
    }
}


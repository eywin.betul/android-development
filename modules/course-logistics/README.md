# Module 1.1 Course Logistics

            
<!-- END COMMON HEADER -->

## Introduction

Welcome to Android Development! In this module, I'll introduce myself and walk through the always amusing logistics of the course!

## Objectives

* Staying awake during this first lecture
* Getting a feel for the way I grade the course
* Making sure you update your information in SIS
* Making sure you read the entire syllabus

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 29:51

            
#### Lecture: Course Logistics (Fall 2021) (29:51)
 | [![Lecture: Course Logistics (Fall 2021)](https://img.youtube.com/vi/yLGIrjAk1Qg/mqdefault.jpg)](https://youtu.be/yLGIrjAk1Qg) |
 | -- |

                

            
package com.javadude.composebasics

import android.util.Log
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun Label(
    text: String,
    modifier: Modifier = Modifier
) = CommonText(
    text = text,
    style = MaterialTheme.typography.h6,
    startPadding = 8.dp,
    modifier = modifier
)

@Composable
fun Display(
    text: String,
    modifier: Modifier = Modifier
) = CommonText(
    text = text,
    style = MaterialTheme.typography.h5,
    startPadding = 24.dp,
    modifier = modifier
)

@Composable
fun CommonText(
    text: String,
    style: TextStyle,
    startPadding: Dp,
    modifier: Modifier
) {
    Log.d("!!!!!", "CommonText(text=$text called")
    Text(
        text = text,
        style = style,
        modifier = modifier
            .padding(
                start = startPadding,
                end = 8.dp,
                top = 8.dp,
                bottom = 8.dp
            )
            .fillMaxWidth()
    )
}

@Composable
fun BoxedText(
    name: String,
    onClick: () -> Unit
) {
    Log.d("!!!!!", "BoxedText(name=$name) called")

    Text(
        text = name,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .border(
                width = 2.dp,
                color = MaterialTheme.colors.primary
            )
            .padding(8.dp)
            .clickable { onClick() }
    )
}
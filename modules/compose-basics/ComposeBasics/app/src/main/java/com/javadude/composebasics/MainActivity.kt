package com.javadude.composebasics

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.javadude.composebasics.ui.theme.ComposeBasicsTheme

@Immutable
data class Movie(
    val title: String,
    val description: String
)

@Immutable
class ImmutableList<T>(val list:List<T>): List<T> by list

public fun <T> immutableListOf(vararg elements: T): ImmutableList<T>
    = ImmutableList(listOf(*elements))


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        var name = "Michael"
//        var name by mutableStateOf("Michael")
        var movie by mutableStateOf<Movie?>(null)
        var movies = immutableListOf(
            Movie("title1", "description1"),
            Movie("title2", "description2"),
            Movie("title3", "description3"),
            Movie("title4", "description4"),
        )

        setContent {
            Log.d("!!!!!", "setContent called")
            ComposeBasicsTheme {
                Log.d("!!!!!", "ComposeBasicsTheme called")
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Log.d("!!!!!", "Surface called")
                    Column {
                        Log.d("!!!!!", "Column called")
                        movie?.let {
                            MovieDisplay(movie = it)
                        }
                        BoxedText(name = "The Sting") {
                            movie = Movie(
                                title = "The Sting",
                                description = "Con guys con non con guys who think they're con guys"
                            )
//                                .apply {
//                                movies = listOf(this) + movies
//                            }
                        }
                        BoxedText(name = "Superman") {
                            movie = Movie(
                                title = "Superman",
                                description = "Christopher Reeves flies!!!"
                            )
//                                .apply {
//                                movies = listOf(this) + movies
//                            }
                        }
                        MovieList(movies = movies)
                    }
                }
            }
        }
    }
}

@Composable
fun MovieList(
    movies: ImmutableList<Movie>
) {
    Log.d("!!!!!", "MovieList called")
    Column(
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        movies.forEach {
            Box(
                modifier = Modifier
                    .padding(8.dp)
                    .border(width = 2.dp, color = Color.Blue)
                    .padding(8.dp)
            ) {
                MovieDisplay(movie = it)
            }
        }
    }
}

@Composable
fun MovieDisplay(
    movie: Movie
) {
    Log.d("!!!!!", "MovieDisplay($movie) called")
    Column {
        Label(text = "Title")
        Display(text = movie.title)
        Label(text = "Description")
        Display(text = movie.description)
    }
}


//@Preview(showBackground = true)
//@Composable
//fun DefaultPreview() {
//    ComposeBasicsTheme {
//        Greeting("Android")
//    }
//}
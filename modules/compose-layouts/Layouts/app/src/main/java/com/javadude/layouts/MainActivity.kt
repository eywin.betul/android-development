package com.javadude.layouts

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.Stable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.ParentDataModifier
import androidx.compose.ui.platform.InspectorValueInfo
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.debugInspectorInfo
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.*
import com.javadude.layouts.ui.theme.LayoutsTheme
import kotlin.math.max

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LayoutsTheme {
                Surface(color = MaterialTheme.colors.background) {
//                    FormUsingColumnWithRows()
//                    FormUsingRowWithColumns()
//                    FormUsingConstraintLayout()
//                    FormUsingFormLayout()
                    SampleBorderLayout()
                }
            }
        }
    }
}


@Composable
fun FormUsingFormLayout() {
    FormLayout(maxLabelWidth = 200.dp, minWidthForSideBySide = 700.dp) {
        Label("First Name")
        Edit("Scott")
        Label("Last Name")
        Edit("Stanchfield")
        Label("Age")
        Edit("54")
        Label("Street")
        Edit("123 Sesame")
        Label("City")
        Edit("New York")
        Label("State")
        Edit("NY")
        Label("Zip")
        Edit("10101")
    }
}

@Composable
fun FormUsingConstraintLayout() {
    ConstraintLayout(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            .wrapContentHeight()
            .verticalScroll(rememberScrollState())
    ) {
        val (l1,l2,l3,l4,l5,l6,l7,e1,e2,e3,e4,e5,e6,e7) = createRefs()

        val barrier = createEndBarrier(l1,l2,l3,l4,l5,l6,l7)

        createVerticalChain(l1,l2,l3,l4,l5,l6,l7, chainStyle = ChainStyle.Spread)
        createVerticalChain(e1,e2,e3,e4,e5,e6,e7, chainStyle = ChainStyle.Spread)

        ConstrainedLabel(l1, "First Name") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e1, "Scott") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            width = Dimension.fillToConstraints
        }
        ConstrainedLabel(l2, "Last Name") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e2, "Stanchfield") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l3, "Age") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e3, "54") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l4, "Street") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e4, "123 Sesame") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l5, "City") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e5, "New York") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l6, "State") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e6, "NY") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l7, "Zip") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e7, "10101") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
    }
}

@Composable
fun FormUsingColumnWithRows() {
    Column {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("First Name")
            Edit("Scott")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("Last Name")
            Edit("Stanchfield")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("Age")
            Edit("54")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("Street")
            Edit("123 Sesame")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("City")
            Edit("New York")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("State")
            Edit("NY")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label("Zip")
            Edit("10101")
        }
    }
}

@Composable
fun FormUsingRowWithColumns() {
    Row(
//        Modifier.verticalScroll(rememberScrollState())
    ) {
        Column(
            verticalArrangement = Arrangement.SpaceAround,
            modifier = Modifier.fillMaxHeight()
        ) {
            Label("First Name")
            Label("Last Name")
            Label("Age")
            Label("Street")
            Label("City")
            Label("State")
            Label("Zip")

        }
        Column(
            verticalArrangement = Arrangement.SpaceAround,
            modifier = Modifier.fillMaxHeight()
        ) {
            Edit("Scott")
            Edit("Stanchfield")
            Edit("54")
            Edit("123 Sesame")
            Edit("New York")
            Edit("NY")
            Edit("10101")
        }
    }
}


@Composable
fun Label(
    text: String,
    modifier: Modifier = Modifier
) {
    Text(
        text = text,
        style = MaterialTheme.typography.h6,
        modifier = modifier.padding(8.dp)
    )
}

@Composable
fun ConstraintLayoutScope.ConstrainedLabel(
    ref: ConstrainedLayoutReference,
    text: String,
    constraintBlock: ConstrainScope.() -> Unit
) {
    Text(
        text = text,
        style = MaterialTheme.typography.h6,
        modifier = Modifier
            .padding(8.dp)
            .constrainAs(ref, constraintBlock)
    )
}

@Composable
//fun RowScope.Edit(
fun Edit(
    text: String,
    modifier: Modifier = Modifier
) {
    OutlinedTextField(
        value = text,
        onValueChange = {},
        textStyle = MaterialTheme.typography.h5,
        modifier = modifier
            .padding(8.dp)
            .fillMaxWidth()
//            .weight(1f) // only if used in RowScope or ColumnScope
    )
}


@Composable
fun ConstraintLayoutScope.ConstrainedEdit(
    ref: ConstrainedLayoutReference,
    text: String,
    constraintBlock: ConstrainScope.() -> Unit
) {
    OutlinedTextField(
        value = text,
        onValueChange = {},
        textStyle = MaterialTheme.typography.h5,
        modifier = Modifier
            .padding(8.dp)
            .constrainAs(ref) {
                constraintBlock()
                width = Dimension.fillToConstraints
            }
    )
}

@Composable
fun FormLayout(
    minWidthForSideBySide: Dp,
    maxLabelWidth: Dp,
    content: @Composable () -> Unit
) {
    with(LocalDensity.current) {
        val maxLabelWidthPx = maxLabelWidth.toPx()
        val minWidthForSideBySidePx = minWidthForSideBySide.toPx()

//        BoxWithConstraints {
//            if (maxWidth >= minWidthForSideBySide) {
//                // FormLayout
//            } else {
//                // Column
//            }
//        }
        Layout(
            content = content,
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) { measurables, constraints ->

            check(measurables.size % 2 == 0) { "FormLayout requires an even number of Composables" }

            if (constraints.maxWidth >= minWidthForSideBySidePx) {

                val labels = measurables.filterIndexed { n, _ -> n % 2 == 0 }
                val controls = measurables.filterIndexed { n, _ -> n % 2 == 1 }

                val labelConstraints = Constraints(
                    minWidth = 0,
                    minHeight = 0,
                    maxWidth = maxLabelWidthPx.toInt(),
                    maxHeight = Constraints.Infinity,
                )
                // 1: measure - determine the sizes of the components
                val labelPlaceables = labels.map {
                    it.measure(labelConstraints)
                }

                val labelsWidth = labelPlaceables.maxOf { it.width }
                val controlsWidth = constraints.maxWidth - labelsWidth

                val controlConstraints = Constraints(
                    minWidth = controlsWidth,
                    minHeight = 0,
                    maxWidth = controlsWidth,
                    maxHeight = Constraints.Infinity,
                )

                val controlPlaceables = controls.map {
                    it.measure(controlConstraints)
                }

                val height = max(
                    labelPlaceables.sumOf { it.height },
                    controlPlaceables.sumOf { it.height },
                )

                // 2: layout - places components
                layout(constraints.maxWidth, height) {
                    var y = 0
                    labelPlaceables.forEachIndexed { n, label ->
                        val control = controlPlaceables[n]

                        val rowHeight = max(label.height, control.height)
                        val labelOffsetY = (rowHeight - label.height) / 2
                        val controlOffsetY = (rowHeight - control.height) / 2
                        label.placeRelative(0, y + labelOffsetY)
                        control.placeRelative(labelsWidth, y + controlOffsetY)
                        y += rowHeight
                    }

                }

            } else {
                val newConstraints =
                    Constraints(
                        minWidth = constraints.maxWidth,
                        maxWidth = constraints.maxWidth,
                        minHeight = 0,
                        maxHeight = Constraints.Infinity,
                    )

                val placeables = measurables.map {
                    it.measure(newConstraints)
                }

                val height = placeables.sumOf { it.height }

                layout(constraints.maxWidth, height) {
                    var y = 0
                    placeables.forEach {
                        it.placeRelative(0, y)
                        y += it.height
                    }
                }
            }
        }
    }
}

@Composable
fun SampleBorderLayout() {
    BorderLayout(
        modifier = Modifier.fillMaxSize(),
        north = { Text("NORTH", modifier = it.background(Color.Blue))},
        south = { Text("SOUTH", modifier = it.background(Color.Blue))},
        east = { Text("EAST", modifier = it.background(Color.Green))},
        west = { Text("WEST", modifier = it.background(Color.Green))},
        center = { Text("CENTER", modifier = it.background(Color.Red))},
    )
}

@Composable
fun BorderLayout(
    north: @Composable ((Modifier) -> Unit)? = null,
    south: @Composable ((Modifier) -> Unit)? = null,
    east: @Composable ((Modifier) -> Unit)? = null,
    west: @Composable ((Modifier) -> Unit)? = null,
    center: @Composable ((Modifier) -> Unit)? = null,
    modifier: Modifier,
) {
    Column(modifier = modifier) {
        north?.let {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                it(Modifier.fillMaxWidth())
            }
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .weight(1f)) {
            west?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .wrapContentWidth()
                ) {
                    it(Modifier.fillMaxHeight())
                }
            }
            center?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(1f)
                ) {
                    it(Modifier.fillMaxSize())
                }
            }
            east?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .wrapContentWidth()
                ) {
                    it(Modifier.fillMaxHeight())
                }
            }
        }
        south?.let {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                it(Modifier.fillMaxWidth())
            }
        }
    }
}

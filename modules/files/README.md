# Module 13.1 Files

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

Many programs need to save or load data to/from files. Android file I/O is similar to Java I/O (using the same APIs), but depending on where you want to save/load files, you may need to jump through a few hoops in the name of user privacy and protection.

In this module, we'll look at simple file I/O in private directories on a device or SD card, and more complex Storage-Access Framework file I/O.

> Note: Due to out-of-town trips, I decided to edit a recording of a live lecture that I gave Summer 2021.

## Objectives

* Learn how to save data privately for an application
* Understand when files will be automatically deleted when an application is uninstalled
* Save data that survives uninstallation and is accessible to other applications.

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:59:57

            
#### Files: Lecture (Summer 2021) (27:29)
 | [![Files: Lecture (Summer 2021)](https://img.youtube.com/vi/8mR8lqTETtE/mqdefault.jpg)](https://youtu.be/8mR8lqTETtE) |
 | -- |

                

#### Files: Example 1 - Private Files (Summer 2021) (35:04)
 | [![Files: Example 1 - Private Files (Summer 2021)](https://img.youtube.com/vi/4g3ScXm31tg/mqdefault.jpg)](https://youtu.be/4g3ScXm31tg) |
 | -- |

                

#### Files: Example 2 - SD Card Private Files and MediaStore (Summer 2021) (35:52)
 | [![Files: Example 2 - SD Card Private Files and MediaStore (Summer 2021)](https://img.youtube.com/vi/EAJPl4xdAxY/mqdefault.jpg)](https://youtu.be/EAJPl4xdAxY) |
 | -- |

                

#### Files: Example 3 - Storage Access Framework (Summer 2021) (21:32)
 | [![Files: Example 3 - Storage Access Framework (Summer 2021)](https://img.youtube.com/vi/Fr_XW94jPIQ/mqdefault.jpg)](https://youtu.be/Fr_XW94jPIQ) |
 | -- |

                

            
package com.javadude.composenfc

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun SimpleButton(
    text: String,
    onClick: () -> Unit
) {
    Button(onClick = onClick, modifier = Modifier.padding(8.dp)) {
        Text(text = text)
    }
}

/**
 * Sets up a text with "display" styling and padding for the app
 *
 * @param text The text to display
 * @param modifier Additional modifiers to pass in
 */
@Composable
fun Display(
    text: String,
    modifier: Modifier = Modifier
) = CommonText(
    text = text,
    style = MaterialTheme.typography.h5,
    startPadding = 24.dp,
    modifier = modifier
)

/**
 * Common text setup used by other more specific text @Composables
 *
 * @param text The text to display
 * @param style The style to use on the text
 * @param startPadding How much space to add before the text
 * @param modifier Additional modifiers to pass in
 */
@Composable
private fun CommonText(
    text: String,
    style: TextStyle,
    startPadding: Dp,
    modifier: Modifier
) {
    Text(
        text = text,
        style = style,
        modifier = modifier
            .padding(
                start = startPadding,
                end = 8.dp,
                top = 8.dp,
                bottom = 8.dp
            )
            .fillMaxWidth()
    )
}

/**
 * Manages common editable text field function
 *
 * Note that we're working around a bug on pressing "back" that currently (as of Compose 1.0.2)
 *   requires the user to press back an extra time to remove focus on a text field before
 *   they can go back from the current screen
 *
 * @param label The text to display as a label
 * @param value The current value to display on the text field
 * @param onValueChange Called to tell the caller that the value has changed
 * @param onBack Needed for the workaround - called to override Back handling
 */
@ExperimentalComposeUiApi
@Composable
fun CommonTextField(
    label: String,
    value: String,
    onValueChange: (String) -> Unit,
    onBack: () -> Unit
) {
    OutlinedTextField(
        value = value,
        label = { Text(text = label)},
        onValueChange = onValueChange,
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            // workaround for bug: https://issuetracker.google.com/issues/192433071
            // for the moment, we'll just force it to consume the Back key in onKeyEvent
            //    and explicitly pop the stack in the navigator
            // a fix for the bug has been submitted but per the comments it sounds like it won't make the 1.0.0 release
            // NOTE: the "Back" key reference is currently experimental API and could change
            .onKeyEvent {
                if (it.key == Key.Back) {
                    onBack()
                    true
                } else {
                    false
                }
            }
    )
}

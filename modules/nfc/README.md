# Module 11.1 Near-Field Communication (NFC)

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

> Note that these videos are from 2016 and were based in Java using Android Views for the UI. I have updated the example to Kotlin using Jetpack Compose for the UI.
> The concepts in the lecture and example videos still apply. (It also might be interesting to watch these older videos to see how Android development looks in Java compared to Kotlin).

> Also note that these examples will not run on the emulator; you can only use NFC on a device that supports it. I would have loved to include an assignment on NFC, but not all students
> will have Android devices, or Android devices with NFC.

Near-Field Communication (NFC) allows your application to read and write hardware tags with a small chunk of data. This data can be plain text, a URI, contact information, or any other data format you would like.
This can be useful, for example, to read data from a poster or business card or write data to a bag describing its contents.

## Objectives

* Understand the basic concepts of Near-Field Communication
* Write text or a URI to a tag
* Read a tag and display its data

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 50:53

            
#### Near-Field Communication: Lecture (Spring 2016) (19:11)
 | [![Near-Field Communication: Lecture (Spring 2016)](https://img.youtube.com/vi/wXEjjljdNWI/mqdefault.jpg)](https://youtu.be/wXEjjljdNWI) |
 | -- |

                

#### Near-Field Communication: Reading Example (Spring 2016) (16:16)
 | [![Near-Field Communication: Reading Example (Spring 2016)](https://img.youtube.com/vi/QzphwRdJ7r0/mqdefault.jpg)](https://youtu.be/QzphwRdJ7r0) |
 | -- |

                

#### Near-Field Communication: Writing Example (Spring 2016) (15:26)
 | [![Near-Field Communication: Writing Example (Spring 2016)](https://img.youtube.com/vi/mUVdV9EmIQs/mqdefault.jpg)](https://youtu.be/mUVdV9EmIQs) |
 | -- |

                

            
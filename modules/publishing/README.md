# Module 14.3 Publishing Apps

            
<!-- END COMMON HEADER -->

## Introduction

What good is an application if you keep it all to yourself? You can use the Google Play Store to distribute your application to others!

In this module, we'll work on uploading a release of an application on the play store and talk about some of the necessary setup and application details you may want to consider when publishing an application.

Note that you do not need to use the Google Play Store for distribution. You can publish an app on a web site for download, send it via email or on a USB drive. However, users must grant permission on their device to be able to install from non-Google-Play-Store locations.

## Objectives

* Prepare a release for the Google Play Store
* Configure the application entry on the Play Store
* Publish a release of the application

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 28:00

            
#### Publishing: Lecture (Summer 2021) (28:00)
 | [![Publishing: Lecture (Summer 2021)](https://img.youtube.com/vi/ouP17DyQsOM/mqdefault.jpg)](https://youtu.be/ouP17DyQsOM) |
 | -- |

                

            
# Module 1.4 Activities

            
<!-- END COMMON HEADER -->

## Introduction

Activites are the main "screens" of your application. You can have a single activity and swap out the user-interface elements (which will be our strategy for this course), or navigate between multiple activities.

In this module, we'll look at activities from a high level and take a quick peek at how the "old school" View setup works. We won't be using Views (other than Google Map) for this class, but you should be familiar with them.

## Objectives

* Understand how the Android system manages the lifecycle of an Activity, and some of the callbacks you can implement to become aware of changes in Activity state
* Shudder when you see the XML that you used to have to write for user interfaces.
* Smile knowing that the newer ways of writing user interfaces are much simpler and more direct.

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:11:14

            
#### Lecture: Activities (Fall 2021) (24:30)
 | [![Lecture: Activities (Fall 2021)](https://img.youtube.com/vi/sMr7xA5GQYE/mqdefault.jpg)](https://youtu.be/sMr7xA5GQYE) |
 | -- |

                

#### Example: Activities (Fall 2021) (46:44)
 | [![Example: Activities (Fall 2021)](https://img.youtube.com/vi/fFewMJHoYF4/mqdefault.jpg)](https://youtu.be/fFewMJHoYF4) |
 | -- |

                

            
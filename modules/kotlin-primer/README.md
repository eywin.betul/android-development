# Module 2.1 Kotlin Primer

            
<!-- END COMMON HEADER -->

## Introduction

Kotlin is a fantastic new language that we'll be using to create our Android Applications. In this module, we'll quickly explore the basics of the language from a Java perspective.

During the rest of the course, I'll often explain some of these concepts in more detail.

## Objectives

* Understand the basic structure of Kotlin classes, properties and functions
* Use lambdas as arguments to functions as callbacks or observers

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 1:13:05

            
#### Example: Kotlin Primer (Fall 2021) (1:13:05)
 | [![Example: Kotlin Primer (Fall 2021)](https://img.youtube.com/vi/wIhwlYdO7cQ/mqdefault.jpg)](https://youtu.be/wIhwlYdO7cQ) |
 | -- |

                

            
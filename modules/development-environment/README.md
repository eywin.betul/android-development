# Module 1.2 Development Environment

            
<!-- END COMMON HEADER -->

## Introduction

Before we can do anything else, you'll need to get Android Studio, the bane of my existence (I miss using Eclipse!), up and running. In this module, I'll describe how to set up the environment and create your first project.

## Objectives

* Understand how to install Android Studio
* Understand how to create a simple Android project
* Understand how to run a project on an Android emulator

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 34:49

            
#### Lecture: Development Environment (Fall 2021) (03:35)
 | [![Lecture: Development Environment (Fall 2021)](https://img.youtube.com/vi/Cuk1hkPEWdQ/mqdefault.jpg)](https://youtu.be/Cuk1hkPEWdQ) |
 | -- |

                

#### Example: Development Environment (Fall 2021) (31:14)
 | [![Example: Development Environment (Fall 2021)](https://img.youtube.com/vi/sxTBIIJAIug/mqdefault.jpg)](https://youtu.be/sxTBIIJAIug) |
 | -- |

                

            
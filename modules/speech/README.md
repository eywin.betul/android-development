# Module 9.2 Speech

            
## Prerequisites
This module assumes that you have already completed the following modules:

* [Module 1.4: Activities](../modules/activities)
* [Module 3.1: Jetpack Compose Basics](../modules/compose-basics)
                
<!-- END COMMON HEADER -->

## Introduction

Android applications can speak to you, and allow you to speak with them!

In this module, we'll look at the Android text-to-speech and speech-to-text APIs, and build a simple text-adventure game that can be played without typing!

## Objectives

* Report application events by speaking the text to the user
* Interact with an application by using your voice
* Get a taste of how you can parse simple commands that the user has spoken

<!-- START COMMON FOOTER -->
## Learning Guide

Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.

Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!

Please post any questions to the Q & A section of the Discussion Board.

## Videos

Total video time for this module: 51:23

            
#### Speech: Overview (Fall 2021) (15:35)
 | [![Speech: Overview (Fall 2021)](https://img.youtube.com/vi/uTMbNvtmwZY/mqdefault.jpg)](https://youtu.be/uTMbNvtmwZY) |
 | -- |

                

#### Speech: Example (Fall 2021) (35:48)
 | [![Speech: Example (Fall 2021)](https://img.youtube.com/vi/YWtooRFWmZ8/mqdefault.jpg)](https://youtu.be/YWtooRFWmZ8) |
 | -- |

                

            
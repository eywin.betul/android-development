import java.io.File
import java.nio.file.Paths
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlinx.datetime.DatePeriod
import kotlinx.datetime.LocalDate
import kotlinx.datetime.Month
import kotlinx.datetime.plus

// TODO ADD DEPENDENCY CHECKING

interface CanOutline {
    fun outline(index: Int): String
}
interface Content {
    fun commonHeader(): String
    fun commonFooter(): String
    fun updateFile(file: File) {
        var state = ContentState.BeforeEndHeader
        val newContent = file.useLines { lines ->
            lines.map { line ->
                when (state) {
                    ContentState.BeforeEndHeader -> {
                        if (line == endCommonHeader) {
                            state = ContentState.InContent
                            "${commonHeader().trimMargin()}\n$endCommonHeader"
                        } else {
                            null
                        }
                    }
                    ContentState.InContent -> {
                        if (line == startCommonFooter) {
                            state = ContentState.AfterStartFooter
                            "$startCommonFooter\n${commonFooter().trimMargin()}"
                        } else {
                            line
                        }
                    }
                    ContentState.AfterStartFooter -> null
                }
            }.filterNotNull().joinToString("\n")
        }
        file.writeText(newContent)
    }
}

val String.indent get() = replace("\\|".toRegex(), "|   ")

val <T : CanOutline> List<T>.outline get() = mapIndexed { index, item -> item.outline(index) }.joinToString("\n")

enum class Term { Fall, Spring, Summer }

@ExperimentalTime
data class Semester(
    val term: Term,
    val year: Int,
    val start: LocalDate
): CanOutline {
    val weeks = mutableListOf<Week>()
    fun breakWeek(title: String) = BreakWeek(title).apply {
        weeks.add(this)
    }
    fun Int.week(init: ClassWeek.() -> Unit) = ClassWeek(this).apply {
        weeks.add(this)
        init()
    }

    override fun outline(index: Int) = """
        ${weeks.outline}
        
        Total Video Time: ${totalTime.videoTime}
    """.trimIndent()

    fun findModule(id: ModuleId): VideoModule =
        weeks
            .filterIsInstance<ClassWeek>()
            .flatMap { it.modules }
            .filterIsInstance<VideoModule>()
            .find { it.id == id }
            ?: throw IllegalStateException("Cannot find module ${id.id} in semester")
}

sealed class Week: CanOutline {
    lateinit var start: LocalDate
    lateinit var end: LocalDate
    open val rows: Int
        get() = 0
}

@ExperimentalTime
class ClassWeek(val number: Int): Week() {
    val modules = mutableListOf<Module>()
    var assignment: Assignment? = null
    var assignmentDue: Assignment? = null
    fun assignment(init: Assignment.() -> Unit) {
        assignment = Assignment().apply(init)
    }

    fun module(id: ModuleId, title: String, init: VideoModule.() -> Unit) =
        VideoModule(id, title).apply {
            modules.add(this)
            init()
        }

    fun tbd() =
        modules.add(TBDModule())

    override val rows: Int
        get() = modules.size + 1 + (if (assignment != null) 1 else 0) + (if (assignmentDue != null) 1 else 0)

    override fun outline(index: Int) =
        """
            |### Week $number &nbsp;&nbsp;&nbsp; ${start.toString().replace('-', '/')} to ${end.toString().replace('-', '/')} &nbsp;&nbsp;&nbsp; (Video Runtime: ${totalTime.videoTime})
            ${modules.outline.indent}
            ${assignment?.start()?.indent ?: ""}
            ${assignmentDue?.submit()?.indent ?: ""}
            |-------
        """
}

data class BreakWeek(val title: String): Week() {
    override fun outline(index: Int) =
        """
            |### $title
            |
            |-------
        """
}

sealed class Module: CanOutline {
    var number: String = ""
    lateinit var week: Week
}

class TBDModule: Module() {
    override fun outline(index: Int) =
        """
            |* Module $number - TBD
        """
}

@ExperimentalTime
class VideoModule(
    val id: ModuleId,
    val title: String,
): Module(), Content {
    val dependencies = mutableListOf<ModuleId>()
    val videos = mutableListOf<Video>()
    val slides = mutableListOf<Slides>()
    lateinit var semester: Semester
    fun dependsOn(id: ModuleId) = dependencies.add(id)
    fun video(youtubeId: String, title: String, duration: Duration) = videos.add(Video(youtubeId, title, duration))
    fun slides(id: String, title: String) = slides.add(Slides(id, title))
    override fun outline(index: Int) =
        """
            |* [Module $number - $title](modules/${id.id}) (Video Runtime: ${videos.totalTime.videoTime})
        """
    fun prereqs() =
        dependencies.dependencyList(semester).let {
            if (it.isNotEmpty()) {
                """
                    |## Prerequisites
                    |This module assumes that you have already completed the following modules:
                    |
                    |$it
                """
            } else {
                ""
            }
        }

    override fun commonHeader() =
        """
            |# Module $number $title
            |
            ${prereqs()}
        """

    fun writeInitialFile(file: File) {
        file.writeText(
            """
                ${commonHeader()}
                |$endCommonHeader
                |
                |## Introduction
                |
                |(TBD)
                |
                |## Objectives
                |
                |(TBD)
                |
                |$startCommonFooter
                ${commonFooter()}
            """.trimMargin()
        )
    }

    fun videoList() =
        if (videos.isEmpty()) {
            "|(Videos in development)"

        } else {
            videos.joinToString("\n") {
                """
                    |#### ${it.title} (${it.duration.videoTime})
                    | | [![${it.title}](https://img.youtube.com/vi/${it.youtubeId}/mqdefault.jpg)](https://youtu.be/${it.youtubeId}) |
                    | | -- |
                    |
                """
//                    | | [![${it.title}](https://img.youtube.com/vi/${it.youtubeId}/maxresdefault.jpg)](https://youtu.be/${it.youtubeId}) |
            }
        }

    fun slidesList() =
        if (slides.isNotEmpty()) {
            slides.joinToString("\n") {
                """
                    |## Slides
                    |
                    |* [${it.title}](${it.id})
                """.trimMargin()
            }
            ""
        } else {
            ""
        }

    override fun commonFooter() =
        """
            |## Learning Guide
            |
            |Begin this module by watching all of the recorded lectures below. I strongly recommend that you watch them in the order listed, as some lectures assume knowledge gained from earlier lectures.
            |
            |Refer to the Course Outline (in the README.md at the root of this repository) for any assignment start or due dates!
            |
            |Please post any questions to the Q & A section of the Discussion Board.
            |
            |## Videos
            |
            |Total video time for this module: ${videos.totalTime.videoTime}
            |
            ${videoList()}
            |
            ${slidesList()}
        """
}

// LIST VIDEOS ON MODULE PAGES
//            |${if (videos.isEmpty()) "   * Videos in Development" else "   * Videos"}
//            ${videos.outline.indent.indent}

@ExperimentalTime
data class Video(
    val youtubeId: String,
    val title: String,
    val duration: Duration
)

data class Slides(
    val id: String,
    val title: String
)

@ExperimentalTime
fun List<ModuleId>.dependencyList(semester: Semester) =
    joinToString("\n") {
        val module = semester.findModule(it)
        "* [Module ${module.number}: ${module.title}](../modules/${module.id.id})"
    }

@ExperimentalTime
class Assignment: Content {
    var id: String = ""
    var title: String = ""
    var number: Int = 1
    lateinit var semester: Semester
    val dependencies = mutableListOf<ModuleId>()
    fun dependsOn(id: ModuleId) = dependencies.add(id)
    lateinit var start: LocalDate
    lateinit var due: LocalDate
    var projectNameText: (n: Int) -> String = { n ->
        "NOTE: Be sure to name this project HW$n and all packages starting with lastname.firstname.hw$n."
    }
    var weeks: Int = 1

    override fun commonHeader() =
        """
            |# Assignment $number - $title
            |
            |## Setup
            |
            |${projectNameText(number)}
            |
            |You must implement this assignment in Kotlin.
            |
            |> Note: Be sure the version numbers in your build files match those in the main [README](../../README.md) and [Sample Code Module](../../modules/sample-code).
            |
            |> Note: Implement *only* the functionality stated in the assignment description. 
            |Addition of extra features and icons makes it more difficult to grade assignments on an even level.
            |Follow the stated instructions for which icons to use and how the UI should be structured (unless the
            |assignment explicitly states you can customize).
        """

    override fun commonFooter() =
        """
            |## Submitting Your Assignment
            |To submit your assignment (do this outside IntelliJ/Android Studio):
            |
            |1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
            |1. Find the directory for your project
            |1. Delete the `build` directories from the top-level project folders and from the `app` folder
            |1. Zip the project directories. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw$number`
            |1. Go to the Blackboard course website
            |1. Click on the Assignment Submission link on the left navigation bar
            |1. Click on the Assignment $number link
            |1. Attach your submission and enter any notes
            |1. Be sure to submit your assignment, not just save it!
        """

    fun start() =
        """
            |* START [Assignment $number: $title](assignments/$id)
            |   * DUE $due by 11:59PM Eastern
        """

    fun submit() =
        """
            |* SUBMIT [Assignment $number: $title](assignments/$id)
            |   * DUE $due by 11:59PM Eastern
        """
}

@ExperimentalTime
val Int.hours get() = Duration.hours(this)
@ExperimentalTime
val Int.minutes get() = Duration.minutes(this)
@ExperimentalTime
val Int.seconds get() = Duration.seconds(this)

@ExperimentalTime
fun semester(term: Term, year: Int, month: Month, day: Int, init: Semester.() -> Unit) =
    Semester(term, year, LocalDate(year, month, day)).apply {
        init()
        validateAndRender()
    }

@ExperimentalTime
fun Semester.validateAndRender() {
    val dir = Paths.get("").toAbsolutePath().toFile().parentFile

    weeks.filterIsInstance<ClassWeek>().size.let {
        if (term == Term.Summer) {
            require(it == 12) { "must be 12 video weeks for summer; found $it"}
        } else {
            require(it == 14) { "must be 14 video weeks for fall/spring; found $it" }
        }
    }

    var assignmentNumber = 1
    weeks
        .filterIsInstance<ClassWeek>()
        .forEach { week ->
            week.modules.forEachIndexed { moduleN, module ->
                module.number = "${week.number}.${moduleN+1}"
                module.week = week
                (module as? VideoModule)?.semester = this
            }
            week.assignment?.let {
                it.number = assignmentNumber
                assignmentNumber++
            }
        }

    val oneWeek = DatePeriod(days = 7)
    val weekEnd = DatePeriod(days = 6)
    var currentDate = start

    weeks.forEachIndexed { weekN, week ->
        week.start = currentDate
        week.end = currentDate + weekEnd
        if (week is ClassWeek) {
            week.assignment?.let { assignment ->
                val assignmentFile = File(dir, "assignments/${assignment.id}/README.md")
                assignmentFile.let { require(it.exists()) { "assignment directory $it not found"} }
                val endWeek = weekN + assignment.weeks - 1
                require(endWeek < weeks.size) { "assignment ${assignment.id} due after end of course" }
                require(weeks[endWeek] !is BreakWeek) { "assignment ${assignment.id} is due on a break week" }
                assignment.start = currentDate
                assignment.due = currentDate + DatePeriod(days = 7 * assignment.weeks - 1)
                (weeks[endWeek] as ClassWeek).assignmentDue = assignment
                assignment.semester = this

                assignment.updateFile(assignmentFile)
            }
            require(week.modules.isNotEmpty()) { "week ${week.number} has no modules" }
            week.modules.forEachIndexed { moduleN, module ->
                if (module is VideoModule) {
                    require(module.title.isNotEmpty()) { "module $moduleN in week ${week.number} does not have a title" }
                    val moduleFile = File(dir, "modules/${module.id.id}/README.md")

                    require(moduleFile.exists()) { "module $moduleFile not found"}

                    // update the module
//                    module.writeInitialFile(moduleFile)
                    module.updateFile(moduleFile)
                }
            }
        }
        currentDate += oneWeek
    }

    // warnings
    weeks.forEachIndexed { weekN, week ->
        if (week is ClassWeek) {
            week.modules.forEachIndexed { moduleN, module ->
                if (module is VideoModule) {
                    module.videos.forEachIndexed { videoN, video ->
                        if (video.duration == Duration.ZERO) {
                            println("video $videoN in module $moduleN in week ${week.number} does not have a duration")
                        }
                    }
                }
            }
        }
    }

    // update the outline section of the main readme
    var outlineState = OutlineState.BeforeOutline
    val mainReadme = File(dir, "README.md")
    val newContent = mainReadme.useLines { lines ->
        lines.map { line ->
            when (outlineState) {
                OutlineState.BeforeOutline -> {
                    if (line == startOutline) {
                        outlineState = OutlineState.InOutline
                        startOutline + '\n' +
                        outline(-1).trimMargin().replace("(?m)^[ \t]*\r?\n".toRegex(), "") + '\n' +
                        endOutline

                    } else {
                        line
                    }
                }
                OutlineState.InOutline -> {
                    if (line == endOutline) {
                        outlineState = OutlineState.AfterOutline
                    }
                    null
                }
                OutlineState.AfterOutline -> {
                    line
                }
            }
        }.filterNotNull().joinToString("\n")
    }

    mainReadme.writeText(newContent)
}

fun Int.pad2() =
    toString().padStart(2, '0')

@ExperimentalTime
val Duration.videoTime: String
    get() = toComponents { hours, minutes, seconds, _ ->
        (if (hours > 0) "$hours:" else "") + minutes.pad2() + ":" + seconds.pad2()
    }

@ExperimentalTime
val List<Video>.totalTime: Duration
    get() = fold(Duration.ZERO) { acc, video -> acc + video.duration }

@ExperimentalTime
val ClassWeek.totalTime: Duration
    get() = modules
        .filterIsInstance<VideoModule>()
        .flatMap { it.videos }
        .fold(Duration.ZERO) { acc, video -> acc + video.duration }

@ExperimentalTime
val Semester.totalTime: Duration
    get() = weeks
        .filterIsInstance<ClassWeek>()
        .fold(Duration.ZERO) { acc, week -> acc + week.totalTime }

private enum class OutlineState { BeforeOutline, InOutline, AfterOutline }
private enum class ContentState { BeforeEndHeader, InContent, AfterStartFooter }

private const val startOutline = "<!-- START OUTLINE -->"
private const val endOutline = "<!-- END OUTLINE -->"
private const val endCommonHeader = "<!-- END COMMON HEADER -->"
private const val startCommonFooter = "<!-- START COMMON FOOTER -->"



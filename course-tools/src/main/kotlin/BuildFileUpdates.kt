import java.io.File
import java.nio.file.Paths

fun main() {
    val dir = Paths.get("").toAbsolutePath().toFile().parentFile

    val modulesDir = File(dir, "modules")
    modulesDir.listFiles()?.forEach { module ->
        module
            ?.apply {
                println("Module: $name")
            }
            ?.listFiles()
            ?.filter { it.isDirectory }
            ?.filter { it.name != "KotlinPrimer" }
            ?.forEach { projectDir ->
                println("    Updating sample code project $projectDir")
                val projectName = projectDir.name
                val applicationId = "com.javadude.${projectName.lowercase()}"
                File(projectDir, "build.gradle").writeText(rootBuild())
                File(projectDir, "app/build.gradle").writeText(appBuild(applicationId))
                File(projectDir, "settings.gradle").writeText(settings(projectName))
                File(projectDir, "gradle/wrapper/gradle-wrapper.properties").writeText(gradleWrapperProperties())
                File(projectDir, "modules.gradle").let {
                    if (!it.exists()) {
                        it.writeText("include ':app'\n".trimIndent())
                    }
                }
                File(projectDir, "app/build-dependencies.gradle").let {
                    if (!it.exists()) {
                        it.writeText("""
                            dependencies {
                                implementation libs.bundles.room
                                implementation libs.bundles.base
                                testImplementation libs.bundles.test
                                androidTestImplementation libs.bundles.android.test
                                debugImplementation libs.bundles.debug
                                kapt libs.bundles.kapt
                            }
                            """.trimIndent()
                        )
                    }
                }
            }
    }
}

fun appBuild(applicationId: String) = """
    plugins {
        id 'com.android.application'
        id 'kotlin-android'
        id 'kotlin-kapt'
        id 'kotlin-parcelize'
    }

    android {
        compileSdk 31

        defaultConfig {
            applicationId "$applicationId"
            minSdk 21
            targetSdk 31
            versionCode 1
            versionName "1.0"

            testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
            vectorDrawables {
                useSupportLibrary true
            }
        }

        buildTypes {
            release {
                minifyEnabled false
                proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
            }
        }
        compileOptions {
            sourceCompatibility JavaVersion.VERSION_1_8
            targetCompatibility JavaVersion.VERSION_1_8
        }
        kotlinOptions {
            jvmTarget = '1.8'
            useIR = true
        }
        buildFeatures {
            compose true
        }
        composeOptions {
            kotlinCompilerExtensionVersion libs.versions.compose.get()
        }
        packagingOptions {
            resources {
                excludes += '/META-INF/{AL2.0,LGPL2.1}'
            }
        }
        ${if (applicationId == "com.javadude.activityexample") "viewBinding.enabled = true" else ""}
    }

    apply from: "build-dependencies.gradle"
""".trimIndent()

fun rootBuild() = """
    buildscript {
        repositories {
            google()
            mavenCentral()
        }
        dependencies {
            classpath libs.bundles.plugins
        }
    }

    plugins {
        id "com.github.ben-manes.versions" version "0.39.0"
    }

    task clean(type: Delete) {
        delete rootProject.buildDir
    }

    def isNonStable = { String version ->
        def stableKeyword = ['RELEASE', 'FINAL', 'GA'].any { it -> version.toUpperCase().contains(it) }
        def regex = /^[0-9,.v-]+(-r)?${'$'}/
        return !stableKeyword && !(version ==~ regex)
    }

    // https://github.com/ben-manes/gradle-versions-plugin
    tasks.named("dependencyUpdates").configure {
        rejectVersionIf {
            isNonStable(it.candidate.version)
        }
    }
""".trimIndent()

fun settings(projectName: String) = """
    enableFeaturePreview("VERSION_CATALOGS")

    dependencyResolutionManagement {
        repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
        repositories {
            google()
            mavenCentral()
        }
        versionCatalogs {
            libs {
                from(files('../../libs.versions.toml'))
            }
        }
    }

    rootProject.name = "$projectName"
    apply from: "modules.gradle"
""".trimIndent()

fun gradleWrapperProperties() = """
    distributionBase=GRADLE_USER_HOME
    distributionUrl=https\://services.gradle.org/distributions/gradle-7.3-bin.zip
    distributionPath=wrapper/dists
    zipStorePath=wrapper/dists
    zipStoreBase=GRADLE_USER_HOME
""".trimIndent()


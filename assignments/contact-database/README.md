# Assignment 2 - Contact Manager Database

## Setup

NOTE: Be sure to name this project HW2 and all packages starting with lastname.firstname.hw2.

You must implement this assignment in Kotlin.

> Note: Be sure the version numbers in your build files match those in the main [README](../../README.md) and [Sample Code Module](../../modules/sample-code).

> Note: Implement *only* the functionality stated in the assignment description. 
Addition of extra features and icons makes it more difficult to grade assignments on an even level.
Follow the stated instructions for which icons to use and how the UI should be structured (unless the
assignment explicitly states you can customize).
<!-- END COMMON HEADER -->

Do not attempt to copy the example project and just tweak it. Android Studio can give you unending pain if you try to rename it, and the application you are creating will be significantly simpler than the movie example.

Start a new project and copy over the pieces you need. I would recommend that you copy the the two build.gradles (top-level and the one in app) from the Room example into your project. 
You will need to fix the application id.

## Introduction
In this assignment, you'll create the database for a simple contacts application.

> Note that the relationship between a Contact and an Address is one-to-many. The Movie application example demonstrated a many-to-many relationship between Movies and Actors, using the Role entity.
>
> For your contact application, you won't need an intermediate class to link the Contacts and addresses. You only need to add a contactId property to the AddressEntity class that holds the id of its owning contact.
>
> You can then find the addresses for a contact by querying for addresses that have that contactId (as well as use the Relation annotation to automate fetching a contact and its addresses at the same time - but note that you do not need to do this for the assignment)

## Database 

Create a ContactEntity class using Room annotations containing

* id (String)
* First and Last Name (separate Strings)
* Phone numbers (Strings): Home, Work, Mobile
* Email Address (String)
        
Create an AddressEntity class using Room annotations containing

* id (String)
* type (String) (for "home" address, "work" address, etc)
* street (String)
* city (String)
* state (String)
* zip (String)
* contactId (String) to track the owning contact for this address (one contact to many addresses)
        
Create a single DAO or DAOs for each entity and a ContactDatabase using Room (your choice on how many DAOs you use)

The DAO(s) must include functions to

* insert contacts and addresses

* update contacts and addresses

* delete contacts and addresses

* queries that return Flows for 
    * all contacts
    * all addresses
    * a contact by its id
    * an address by its id
    * all addresses for a contact

Create a ContactRepository interface and a ContactDatabaseRepository that expose all functions from the Dao. Note that
the flows must be exposed as vals in the repository, as demonstrated in the Movie example.

Create a ContactViewModel that provides

* A private property containing an instance of the database

* A public immutable Flow property for a list of all contacts

* A public immutable Flow property for a selected contact

* A public immutable Flow property for a list of addresses for a selected contact (note that you'll need to use flatMapLatest to update this whenever the selected contact changes)

* A public immutable Flow property for a selected address

* Functions to insert, update and delete contacts and addresses

When creating the database, create three dummy contacts, each with two addresses 

## User Interface

Create a simple test user interface similar to the Movie Database example I demonstrated in class

Your application should consist of

* A Text showing all the information for the currently-selected contact. Use a kotlin raw string ("""...""") to format the values on separate lines

* A Text showing all of the information for the currently-selected address.  Use a kotlin raw string ("""...""") to format the values on separate lines

* A Text showing the names of all contacts (one per line, separated by new lines)

* A Text showing the streets of all addresses (one per line, separated by new lines)

* Buttons to		
    * Select contacts one and two (two separate buttons)
    * Select each of the addresses for contact 1 (two separate buttons)
    * Delete contact 1
    * Delete the first address of contact 1
    * Add a new contact with a random UUID as its id

<!-- START COMMON FOOTER -->
## Submitting Your Assignment
To submit your assignment (do this outside IntelliJ/Android Studio):

1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
1. Find the directory for your project
1. Delete the `build` directories from the top-level project folders and from the `app` folder
1. Zip the project directories. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw2`
1. Go to the Blackboard course website
1. Click on the Assignment Submission link on the left navigation bar
1. Click on the Assignment 2 link
1. Attach your submission and enter any notes
1. Be sure to submit your assignment, not just save it!
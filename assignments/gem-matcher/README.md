# Assignment 4 - Gem Matcher Game

## Setup

NOTE: Be sure to name this project HW4 and all packages starting with lastname.firstname.hw4.

You must implement this assignment in Kotlin.

> Note: Be sure the version numbers in your build files match those in the main [README](../../README.md) and [Sample Code Module](../../modules/sample-code).

> Note: Implement *only* the functionality stated in the assignment description. 
Addition of extra features and icons makes it more difficult to grade assignments on an even level.
Follow the stated instructions for which icons to use and how the UI should be structured (unless the
assignment explicitly states you can customize).
<!-- END COMMON HEADER -->

## Introduction

In this assignment, we'll create a simple "Gem Matcher" game.

The game will be similar to an over-simplified Bejeweled, Candy Crush, and similar games. If you're not familiar with any of these, I recommend you download Candy Crush and give it a try.

| ![Gem Matcher with Grid Lines](matcher1.gif) |
| --- |
| *Gem Matcher with Grid Lines* |

## Requirements

* You must implement this assignment in Kotlin using Jetpack Compose and coroutines.

* You must use a Canvas to draw items; DO NOT use a grid layout composable function! All shapes/lines (if any) must be drawn in the same Canvas.
    
* The idea behind the game is to have a grid of random shapes. You can swap any two horizontally or vertically adjacent shapes if it allows you to make a "match"

* You'll need a view model to track the state of the game. I recommend using a list to represent the grid. Note that if you define the following two extension functions, you can use two-dimensional references. (here I'm using 1 for the first row/col)
     
     ```kotlin
    private fun shapeIndex(row: Int, column: Int) = (row-1)*8 + column - 1
       
    operator fun List<Shape>.get(row: Int, column: Int) = this[shapeIndex(row, column)]
        
    private operator fun MutableList<Shape>.set(row: Int, column: Int, shape: Shape) {
        this[shapeIndex(row, column)] = shape
    }
    
    ...
       
    shapes[row, column]
    ```

* Note - I recommend you expose the shapes as an IMMUTABLE list to the user interface so the composable functions cannot directly modify it. In my example I temporarily use a mutable list when doing a more complex operation like shifting the items down, but I always pass a new IMMUTABLE list instance through the flow to the UI. The above `set` function is only for use on that temporary mutable list.
      
* A few helpful functions:

    ```kotlin      
    List(n) { initialValue } // creates a list of size n and the lambda is called to initialize each item
             
    Random.nextInt(4) // returns a random integer from 0 to 3

    // If you decide to track matches as indices, `mapIndexed` is your friend for removing them. For example:
                          
    // extension function for list that creates a new list by replacing items at indices in the 
    //    matches set with Empty shapes - Kotlin is so cool!
    //    called as 
    //       shapes = shapes.removeMatches(matches)
    // you do not have to do things this way (especially if you represent your data differently)
    private fun List<Shape>.removeMatches(matches: Set<Int>) =
        mapIndexed { n, existingShape ->
            if (n in matches) {
                Empty
            } else {
                existingShape
            }
    }
    ```
      
* Three or more adjacent shapes in a row or column of the same type are considered a "match". Note that for any given shape, it's part of a match if it has two adjacent shapes of the same type, and this will automatically cover 4 or more in a row/column as well. Diagnonal doesn't count, nor does a square of four shapes.
   
* If the user has made a move that creates one or more matches
   * Check for all possible matches - the user may have created a row and column or two rows or two columns etc. at the same time
   * Highlight the matching shapes for a moment (perhaps 250-500ms? find an amount that feels right to you). You can highlight by blinking, changing the size of the shape, changing the background color of the cells containing the shape, whatever you would like, but it must be obvious that a match has occurred.
   * Note that the highlight must go away after the blinking is done
   * Remove the matching shapes (blank them out for a moment - the user should see the grid with the matches gone)
   * Add one point to the score for each matched shape (don't add bonus scores for longer matches)
   * Shift all shapes down (you can animate their movement if you want, but you do not need to - you can just redraw all shapes in their new positions). The user should see the grid containing empty spaces only at the top.
   * Add new random shapes to fill in blanks
   * Repeat until no matches remain
      
* At the start of the game, repeatedly do the above match checking and removal until no matches remain. The user should see this process (the same as when they make matches), and it should 
  add to their score.
   
* You must have four different shapes
   * Two can be simple such as a circle and square or rectangle
   * Two must be more complex, defined using Paths. You cannot use a triangle; you must define your own shapes.
   
* Shapes must be separated from each other in the grid (there must be some space between them)
   
* The cells containing shapes must be square, in an 8x8 grid. I recommend the following:
   * Determine the smallest of the width or height in the canvas. (You can use size.width and size.height inside the lambda passed to the Canvas)
   * Divide that value by 10 - use this as the side size of cell squares
   * The shapes should be scaled inside the squares - **DO NOT** assume a specific cell or shape size
   * When drawing, skip past one cell vertically and horizontally to act as a margin

* Your grid may or may not have lines; your call. For example, without grid lines it might looks like

  | ![Gem Matcher Without Gridlines](matcher2.gif) | 
  | -- |
  | *Gem Matcher Without Gridlines* |
   
* User interaction:
   * All user taps/drags can happen anywhere in a grid cell for a shape, NOT just on a shape in that cell. Tapping/dragging inside a cell interacts with the shape inside that cell.
   * To select a cell, **DO NOT** cycle through all shapes and do a bounds test as shown in the graph example. Instead, you can just use integer division to determine which row/col (if any) contains the tap Offset. To ensure you're doing integer division, you can convert a float value to an integer using toInt(). For example, `offset.x.toInt()`
   * Dragging a shape moves it, like in the graph drawing example
   * If a shape is dragged onto another shape's grid area and released
      * If no matches are possible, return the shape to its old position
      * If any matches are possible, swap the two shapes and do the "if the user has made a move that creates one or 
           more matches" logic above 
      * There is no requirement for this assignment to limit the visual dragging of the shape. Iin a real game, you'd lock the move to strictly horizontal or strictly vertical, and not allow it to move freely. For this assignment, we'll keep it simpler and you can drag it anywhere and only test its location when the user releases their finger)
   * Tapping outside the grid "pauses" the game
      * Note - you do not need to check for taps on the score, only in the Canvas that draws the grid (which should take up all remaining space below the score).
      * Just blank the screen and display "Paused"
      * Tapping again unpauses
      * (This doesn't really do anything b/c we're not timing anything...)

        | ![Pausing the Game](matcher4.gif) | 
        | -- |
        | *Pausing the Game* |


   * Dragging outside the grid "shuffles" the pieces
      * Note - you do not need to check for drags on the score, only in the Canvas that draws the grid (which should take up all remaining space below the score).
      * This means starting and ending a drag outside the grid, NOT dragging a piece outside the grid. If a piece is dragged outside the grid it should be returned to its original position when the user releases their finger.
      * To keep this shuffle simple, simply re-add random pieces to all spots in the grid
      * Deduct 10 points when the user shuffles

        | ![Shuffling Pieces](matcher5.gif) | 
        | - |
        | *Shuffling Pieces* | 

   * Tapping/releasing on a cell highlights all other shapes of the type in that cell for a moment (again, 250-500ms?)

     | ![Tapping on a Cell](matcher3.gif) | 
     | -- |
     | *Tapping on a Cell* |

      
* Do not worry about game states where making a match isn't possible (game over). (This can be a little complex to 
   	 compute and I don't want to add in a lot of extra logic requirements)
      
* **Do not** use third-party libraries for this assignment! You should be drawing your shapes and board manually, 
     like in shape editor example.
     
* **Do not** try to force-fit the shape editor example into this game. It demonstrates several techniques that you'll need, but it's not quite the structure you'll want. In particular, don't walk through all the shapes to figure out which has been tapped. 
   
* Ignore screen rotation. You can lock the orientation using the following in the app/src/main/AndroidManifest.xml file:

   ```xml   
   <activity
      android:name=".MainActivity"
      ...
      android:screenOrientation="portrait"
      tools:ignore="LockedOrientationActivity">
   ```
     
<!-- START COMMON FOOTER -->
## Submitting Your Assignment
To submit your assignment (do this outside IntelliJ/Android Studio):

1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
1. Find the directory for your project
1. Delete the `build` directories from the top-level project folders and from the `app` folder
1. Zip the project directories. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw4`
1. Go to the Blackboard course website
1. Click on the Assignment Submission link on the left navigation bar
1. Click on the Assignment 4 link
1. Attach your submission and enter any notes
1. Be sure to submit your assignment, not just save it!
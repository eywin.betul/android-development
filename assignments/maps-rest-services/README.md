# Assignment 5 - Maps, Rest and Services

## Setup

NOTE: Be sure to name these projects HW5_Service and HW5_Client and all packages starting with lastname.firstname.hw5.

You must implement this assignment in Kotlin.

> Note: Be sure the version numbers in your build files match those in the main [README](../../README.md) and [Sample Code Module](../../modules/sample-code).

> Note: Implement *only* the functionality stated in the assignment description. 
Addition of extra features and icons makes it more difficult to grade assignments on an even level.
Follow the stated instructions for which icons to use and how the UI should be structured (unless the
assignment explicitly states you can customize).
<!-- END COMMON HEADER -->

## Introduction

In this assignment, you'll create an application to track an alien invasion. A RESTful web service is provided that tells you the current location of several alien ships. Your application will use this service to ascertain the ship locations and display them on your device using Google Maps.

Your application will use an Android service to fetch changes via the RESTful web service. The Android service will poll for changes once per second.
   
_Note this this a **very** bad way to implement this in real life. Ideally you would use something like Firebase Cloud Messaging to push updates to devices rather than poll. However, I thought it was more important for you to try communicating between an Activity and an Android service than set up Firebase Cloud Messaging._
   
**_The aliens are trying to tell us something! Figure out a way to see what they're saying!_**

| ![Sample Screen](image001.jpg) | 
| -- |
| *Sample Screen* |


## Application Parts

For this assignment, you'll need two application projects. The general communication structure will be similar to the AndroidServices and AndroidServiceClient sample projects. Be sure to watch the quick "remote services example" video descriving them.

   * HW#_Service: application that only contains a service that polls for alien invasion updates using REST
     
     In this project, create a simple Activity that just displays (using Jetpack Compose) a Text stating "Service Project Ready".
     (You don't really need an Activity in this project, but this is a convenient way to install the service project, by running the Activity, and quickly know it's installed)

   * HW#_Client: application to connect to the service and display the map, ship icons, and the paths the ships have taken
   
You will need to copy some parts from one application to another. In a real setup, we'd likely create a separately-compilable library that can be a dependency from both applications.

You will need to create the following  

   * A Parcelable representation of a UFO position

   * AIDL files
      * A reference to the UFO position (UFOPosition.aidl) - it only needs to contain:
       
        ```java
        package lastname.firstname.hw#;

        parcelable UFOPosition;
        ```

      * A reporter interface that the service can use to communicate status updates to the activity (it will pass UFOPosition objects). The report(...) method should pass a List of UFOPosition objects.
      
      * The alien service's interface (allows add/remove of the reporter)

## UFOPosition

This will be a Parcelable class (so it can be passed between the service and the activity) that contain the following information

   * int ship
   * double lat
   * double lon
   
You can annotate this to make it available for automatic conversion from JSON using Retrofit2.  

## The Service

Your service must do the following:

   * Allow Activities to bind to the service
   * Allow reporter instances to be added and removed
   * Start a coroutine to perform the REST requests
      * **_THERE SHOULD BE ONE COROUTINE IN YOUR SERVICE. DO NOT START A SEPARATE COROUTINE FOR EACH REPORTER OR FOR EACH REST CALL._**
      * Once per second, make a request to http://javadude.com/aliens/n.json where "n" is a number starting at 1, and should be incremented for each request. (Note that I'm _simulating_ REST requests via static JSON files on my static web site.) To see an example of the JSON, point your browser to <a href='http://javadude.com/aliens/1.json' target='_blank'>http://javadude.com/aliens/1.json</a>. I recommend you look at a few different 'n's here to see how the ships are tracked over time.
      * Use Retrofit2 to run your REST requests, similar to the REST example in class. 
      * If a request returns a 404 (NOT_FOUND) status code, stop polling. _Do not assume the number of reports that are available._
      * Parse the returned JSON into objects using Retrofit2's automatic JSON support. It will be a List of UFOPositions.
      * Pass the List of UFOPositions to all registered reporters
      
Note: You will see Android complain about plain-text communication; I didn't want to cloud this assignment with HTTPS setup (which is the same as in any Java application). To get around this, add

```xml
android:usesCleartextTraffic="true"
```

to the application  tag in your AndroidManifest.xml


## The Activity

Notes:
   * You should be using Jetpack Compose as the basis for your User Interface, including the MapView as an AndroidView. (The idea is to get used to including old-style Views inside a Jetpack Compose user interface)
   
   * If you're seeing a "U" where you think an "O" should appear in the alien's message, you're doing something wrong...

      * (If you're not seeing a "U" anywhere, you _might_ be doing it right, or might be getting lucky, timing-wise...)

      * Be careful _where_ you call addReporter - make sure you do it only when the map is actually available or you might not be able to add the first report to the map.

      * Be careful _where_ you start the coroutine in your service. You shouldn't start polling _until someone has added a reporter_ to the service. Think about where in the service you know that a reporter has been added. (It should be pretty obvious...). And be careful not to start multiple coroutines!

   * Lock your screen orientation on the activity screenOrientation to the &lt;activity&gt; tag in the manifest:
   
     ```xml   
     <activity
        android:name=".MainActivity"
        ...
        android:screenOrientation="portrait"
        tools:ignore="LockedOrientationActivity">
     ```
   
   * You _do not_ need to worry about saving/restoring data for configuration changes or if the user presses back or home while running the application. These would be necessary in a real application, but this assignment already has a good deal of features to focus upon. In other words, it's ok if everything restarts if back/home is pressed and the application is re-entered.		  
   
   * Your Composable function that sets up the map should ONLY set up the map - it should not take parameters for things to draw on the map, or it's really easy to accidentally re-initialize the map
   
     Basically you'll set up the map view, set a googleMap property in the activity, and you're done there.

     Later, your report function in the activity will directly interact with the googleMap object.

     That's not great from a Compose unidirectional-data-flow perspective, but it's really the safest thing to do until they have a Compose-friendly Google Map...

   * All Google map updates must be done on the UI thread. This can be done in a coroutine, but that's a little trickier because you'd need to get a coroutine scope for the activity...

     Because we're not trying to pass data between threads or synchronize anything, you can set up your reporter function to call `runOnUiThread { ... }` around the google map calls. For example:

         fun report(ufos: List<UFOPosition>) {
             runOnUiThread {
                 // all your report code or just the actual marker and camera calls
             }
         }

     Note that `runOnUiThread` will schedule the lambda to be run when the UI thread is available and will NOT wait (that's the big advantage to coroutines - switching dispatchers using `withContext` will wait before executing the code after it). So any code immediately after the `runOnUiThread { ... }` will run concurrently. You should either put the entire body of the report function inside the `runOnUiThread {...}` or make sure that's the last thing in the report function.

   * To remove markers...
   
     First, you can get a reference to markers you create via

         val marker = googleMap.addMarker { ... }

     Later when you want to remove it, call

         marker.remove()

   
Your activity must do the following:

   * First create an Activity that only displays a map centered on lat=38.9073, lon=-77.0365. Test this to be sure you have properly set up the google maps API and properly registered an Android key. Do not write any other code until you have this working!
   * Bind to your service (you'll need to have the service application already installed for this to work)
   * When the service has been connected _and_ the map is ready, add a reporter to be notified of ship positions. Note that you cannot assume if the service is connected first or the map is ready first. (You may want to wait to bind to the service until the map is ready, then, when the service is ready, add the reporter, but there are other approaches as well)
   * Track the bounding box that will be large enough to contain all UFO positions that you have seen. Use `LatLngBounds` like in the example.
   * When you receive a list of UFOPositions
      * If a position represents a new ship, create a new UFO marker on the map (and whatever data you need to save its positions)
      * If a position represents an existing ship, update its marker's position; note that you should only ever have one marker per ship on the screen.
      * If the ship has been reported for more than one location (across multiple report calls), draw a line on the map representing the path from its last reported position to its current position. _Note: You will use Google Map `PolyLine`s, which have a similar setup to adding markers. You only need to draw PolyLines for each new line; the map will remember the previous lines (until you exit and re-enter)_ It is ok to use multiple Polylines for a single UFO's trail, or you can use a single Polyline with all parts. (Multiple would be a little simpler)
      * If a ship was previously reported but is _not_ in the current report list, remove its marker from the map. _Do not remove the lines that represented its path!_
      * Whenever you see a new position for a UFO, add that position to your bounds so it will expand to include that position. You can do this using

        ```kotlin
        bounds = bounds.including(latLng) // see the example for dealing with the first point!
        ```

      * Update the map to include the new bounds (use a value in a dimens.xml file to set the padding to something reasonable)

        ```kotlin
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
        ```

      * Note: To center your map icons over the proper lat/lng, use
        ```kotlin
        markerOptions.anchor(0.5f, 0.5f);
        ```
      
   * Be sure to unbind from the service when you stop your application
   * Get the UFO icon from [ic_ufo_flying.xml](ic_ufo_flying.xml). (Right-click the link to download; Note that I've tweaked the colors and size) 
     Attribution: Icon made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com)
   * To load the UFO as a BitmapDescriptor, copy the DrawableHelpers.kt file from the maps example project to your project. Then call
      ```kotlin
      loadBitmapDescriptor(R.drawable.ic_ufo_flying)
      ```

<!-- START COMMON FOOTER -->
## Submitting Your Assignment
To submit your assignment (do this outside IntelliJ/Android Studio):

1. Close the project in IntelliJ/Android Studio (and/or quit IntelliJ/Android Studio)
1. Find the directory for your project
1. Delete the `build` directories from the top-level project folders and from the `app` folder
1. Zip the project directories. Be sure to get all files and subdirectories, including those that start with '.'. Be sure to name the zip file `lastname.firstname.hw5`
1. Go to the Blackboard course website
1. Click on the Assignment Submission link on the left navigation bar
1. Click on the Assignment 5 link
1. Attach your submission and enter any notes
1. Be sure to submit your assignment, not just save it!